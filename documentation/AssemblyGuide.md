Assembly Guide, Handbook and User Guide for the USAT antenna rotator developed by TUDSaT

Disclaimer:

This project is still under development therefor no guarantees in any way can be made.

Requirements:

The initial idea was to have a small antenna rotator that can be easily adapted to different needs.
In addition to that it should be cheap yet reliable enough to move small antennas or a small camera.

As those statements are not very precise we came up with the following interpretation of them: These requirements have been accepted by the project initiator Milenko Starcik.

* Price for the rotator and electronic ≤ 50 €
* This does not include a computer to control the rotator or process data.
* Easy to build and repair
* It should be possible to obtain all nessecarry parts and tool to build a rotator without placing special manufacturing orders or alike.
* 3D-Printing is seen as something that radio amateurs, cubesat teams or hobbyists have access to.
* Necessary tools should be easy to obtain and not expensive.
* Reliability
* It should work within the temperature range of 10-30 °C
* Be water resistant such that water can not come in contact with the electronic parts or the movement.
* Physical capability
* Move Antennas up 500 g, with dimensions in the range of 400mm x 150mm x 150mm
* Pointing accuracy of at least 1° in both axis

In addition to that, the whole project should be published under an open source license (GNU GPN v3)

Derived from that those requirements are the following second level requirements:

* All 3D printed parts should be small enough to fit onto an average build plate. For this we looked at the most common printers but also on the cheaper models and settled for an average build plate to be 200mm x 200mm.

To ensure that the rotator can be modified, all 3D files are produced with openscad. The *.scad files are also published.

Parts:

* 3D-Parts:
* 1 x Base
* 1 x Top
* 1 x gear
* 1 x bearingInletA
* 1 x bearingInletB
* 1 x motorBracket

* Other parts
* 3 x M3x20 / M3x30 (Flat / Countersunk)
* 1 x M3 nut (see next item)
* 2 x M3 self securing nuts or 4 x M3 nuts
* 4 x 602 Bearing
* 80-100 x 6mm plastic balls (those for air soft guns)
* 1 x End stop
* 1 x Spring that fits around a M3 screw with a length of about >=15mm (will be cut into two parts)
* Nema 17 stepper motor
* Wires
* Dupont connectors
* Super glue
* Shrinking tube 20 mm, 10 mm both about 4cm (more is bedder)

* Optional Parts
* 3-7 M3x20 M3x30 screws (see next item)
* 2 nuts per screw

Depending on your needs some screws to fix the rotator at its desired position.

Tools:
* A small plyer
* A small screwdriver that fits your screws.
* Some heat source for the shrinking tube
* A knife or scissor to cut the shrinking tube

# Printing the parts

*Note: If you are unfamiliar with 3D printing please look for help at a local fablab or alike.*

As this module is also ment to be placed outdoor print it with a material that is temperature resistant and does not degenerate because of UV-light. We use PETG for the parts. A light color is also recommended.

Base and Top should be printed with a wall count of 3 or 4, infill >= 12 %. Supports are only needed for Top.

Gear, bearingInlet(A/B) and the motorBracket do not need support, infill >= 20 %, wall count 4.

All parts can be printed with 0.3 mm layer height.

Remove excess material and support structures.

# Assembly

## Base

* Place the M3 nut in the bearingInletA and secure it with a bit of super glue. Make sure not to block the thread. Test it with the screw.

* Solder three wires to the end stop. The length is determined by the placement of the control board and the rotator. Solder a Dupont connector to the other end.

* Mount the end stop with two 2.5mm screws in the bottom part. It is positioned on the outer pointing side of its mount. Choose the hole depending on your end stop, the leaver should end about 5mm above the plastic.

* Mount the gear on the nema stepper motor (we refer to it just as motor). it should be flat with the shaft or end a bit below.

* Place the motor in the bottom part. The cables should face the center of the part.

* Cut the spring into two pieces of about 4-8 mm length.

* Place two M3 screws in the hole on the motorBracket, the screw heads should be on the flat round side. Place the parts of the spring on the screws on the opposite side.

* Place the motorBracket on the motor with the screw through the holes next of the motor. Place the self securing nuts on the screws. If you do not have self securing nuts, use two normal nuts to counter each.

* Tighten the screws such that the motor can tilt about 3-4mm inwards. Make sure the screws can not get loose.

* Lead all cables through the cable channel.

* Take one bearing and put bearingInletA in it.

* Place the bearing from the previous step in the "tower" of top part. The nut must be on the inside. It must also be centered. If inserted the wrong way, you can push it out with a M3 screw from the side.

* Drill the holes into top that fit your needs. Insert the needed screws. Make sure no water can leak into the part at those screws. The screws must be flush on the inside.

* Turn the top part such that the bearing is pointing up and place all the ball in the ring around the
big gear.

* Place the bearingInletB on the bearingInletA. It has to interlock with the gaps in the inlet.

* Turn the base and place it on the top part. To allow the gears to interlock tilt base a bit to the side of the motor. Try to turn it,  you should feel the resistance of the stepper motor.

* Insert the M3 screw in the center hole. Push the base down around that screw and tighten it.

* Turn the assembly and make sure the  end stop is activated. The position of the end stop and the rises on the top are marked by a rectangular hole in top. If it does not work open the assembly and mount the end stop upper / tilted.


## Elevation Axis (A)

* Insert one M3 nut into the worm gear. Make shure the hole is in line with the hole for the screw in the plastic.

* Press the worm gear onto the stepper motor shaft. Approximately 5mm of the shaft should still be vosible. Insert a M3-10/15 screw and tighten it.

* Insert the motor with the gear into the motorUnit with 4 M4-10/15 screws. The connector should face inwards. Make shure the gear can turn freely.

* Solder three wires to the endstop. 

* Mount the endstop in the motorUnit. Use two M2-20 /M2.5-20 screws with fitting screws. 

* Cut the M8 rod to your desired length (we recommend ~ 35cm). Put the following parts onto it, mind the order.
Nut, tube-cylinder, tube-shield, 608 bearing, nut, nut, gear, nut, nut, 608 bearing, tube-shield, tube-cylinder, nut. Do not tighten it, but make shure the gear is near the middle of the rod.

* Insert the rod into the motorUnit. To do so, allign the bearing with the bearingHolders in the moroUnit and press the rod it. Hold the nuts and turn / push the rod such that is.

* Temporarily tighten the nuts on the gear.

* Turn the nuts between the gear nuts and the bearing outwards until they press against the bearings.

* Thighten the outward nuts against the tube-cylinders firmly. 

* Unfasten the nuts around the gear.

* Tune the endstop: Hold the rod fixed and use a screw driver to turn the worm gear. This moves the gear on the rod left or right. Tune the position of the gear such that the endstop activates about 80% of hte hight of the bumpers.

* Tighten the gear nuts.

* Place the motorCap ontop the motorUnit. Push it in and secure it with two M3 screws and nuts.

* Attach the stepper cables.

* Mount an ringModule under the motorUnit by using M3 screws and nuts.

* Mount a cableModule unter the ringModule by using M3 screws and nuts.

* Lead the servo and endstop bales through the hole of the cableModule and 
 
 * Use 3 pieces of shrinking tube to  seal the cables to cableModule water tight (see pictures).
 
 
