include <gears.scad>
space = 1.5;
wall = 1;
baseWidth = 190;
totalWidth = baseWidth + 2*space + 2* wall;
ballD = 6;
ballDplusd = ballD + 1;
margin = 1;
skirt = 25;
indicatorExcentricity = 70;
baseHight = 80;

bearingInnerD = 8;
bearingOuterD = 22;
bearingHight = 7;

$fn = 64;// remove /8 for printing

module Nema17(xdim ,ydim ,zdim,rdim,shaft){
	translate([xdim/2,ydim/2,20])
		cylinder(r=shaft,h=25, $fn=50,center=true);
	translate([xdim/2,ydim/2,41/2])
		cylinder(r=11,h=4, $fn=50,center=true);
		difference(){ 
		
			hull(){	
				translate([rdim,rdim,0])
					cylinder(h=zdim,r=rdim, $fn=32,center=true);
				translate([xdim-rdim,rdim,0])
					cylinder(h=zdim,r=rdim, $fn=32,center=true);

				translate([rdim,ydim-rdim,0])
					cylinder(h=zdim,r=rdim, $fn=32,center=true);
				translate([xdim-rdim,ydim-rdim,0])
					cylinder(h=zdim,r=rdim, $fn=32,center=true);
			}
			/*union(){
			translate([36.55,5.75,34])
				cylinder(r=1.5,h=50, $fn=50,center=true);
			translate([36.55,36.55,34])
				cylinder(r=1.5,h=50, $fn=50,center=true);	
			translate([5.75,5.75,34])
				cylinder(r=1.5,h=50, $fn=50,center=true);
			translate([5.75,36.55,34])
				cylinder(r=1.5,h=50, $fn=50,center=true);	
			}
			*/
		}
}

module motorDummy(){
	cube([42.3,42.3,64],center = true);
	translate([-20,0,-64/2 + 7/2])
		cube([13,17,7],center = true);
}

module endStop(){
	cube([20,6.25,10.1]);

	translate([0,0,2.75]){
		rotate([-90,0,0]){
			translate([15,0,0])
				cylinder(d = 2.5, h = 30,center = true);
			translate([5,0,0])
				cylinder(d = 2.5, h = 30,center = true);
		}
	}

	translate([0,0,10])
		rotate([0,-15,0])
			cube([22,5,0.5]);


	translate([2,0,-4]){
		cube([1,3,4]);
		translate([6,0,0])
			cube([1,3,4]);
		translate([9+6,0,0])
			cube([1,3,4]);
	}

	translate([20,0,17])
		rotate([-90,0,0])
			cylinder(d = 3.5, h = 4);
}

module nema(b){
	translate([-42.3/2,-42.3/2,41/2])
	Nema17(42.3,42.3,41,3,2.5);
	if(b){
		translate([0,0,45])
		spur_gear (modul=1, tooth_number=48, width=5, bohrung=4, pressure_angle=20, schraegungswinkel=00, optimized=false);
		
		
	}
}

module nemaX(){
	scale(1.02)
		nema(false);
}

module bottom(){
	a = 16;
	cableChannelH = 30;
	
	difference(){
		union(){
			difference(){
				cylinder(d =baseWidth, h = baseHight,center = true);	
				translate([0,0,baseHight/2  +1])
					rotate_extrude(convexity = 10)
						translate([(baseWidth-ballD)/2 - margin, 0, 0])
							circle(d = ballDplusd,center = true);
				translate([0,0,4])
					cylinder(d = baseWidth-ballDplusd*2 - 2*margin,h = baseHight,center = true);
		
				rotate([0,0,-45])
					translate([0,baseWidth/2,-baseHight/2 +cableChannelH])
				rotate([-90,0,0])
					cylinder(d = a, h = 50,center = true);
		
				translate([0,0,-baseHight/2])
					cylinder(d1 = 14,d2 = 3, h = 8,center = true);
			}
			difference(){
				union(){
					translate([-(indicatorExcentricity - 7.5), 5,0])
						cube([4,20,baseHight - 15],center = true);
					translate([-(indicatorExcentricity - 7.5) + 4,5,-baseHight/2 + 25])
						cube([5,5,40],center = true);
					translate([-(indicatorExcentricity - 7.5),5,-(baseHight/2) + 5])
						cube([20,10,10],center = true);
				}
				
				for(g = [-1:2])
					for(i = [-3:2:3])
						translate([-indicatorExcentricity+5,-5 + i,13 + i + 5*g])
							rotate([0,0,90])
								endStop();
			}
			for(i = [1:3])
				rotate([0,0,i*90 + 45])
					translate([baseWidth / 2 + 5,0,2 - baseHight/2])
						difference(){
							cube([15,15,4],center = true);
							cylinder(d1 = 4,d2 = 10, h = 4,center = true);
						}
			rotate([0,0,-45])
				translate([0,baseWidth/2,-baseHight/2 +cableChannelH])
			difference(){
				rotate([-90,0,0])
					union(){
						translate([0,0,5])
							cylinder(d = a,h = 20,center = true);
						
						translate([0,0,5])
							cylinder(d = a + 2,h = 2,center = true);
						
						translate([0,0,12])
							cylinder(d = a + 2,h = 2,center = true);
					}
				rotate([-90,0,0])
					cylinder(d = a - 2,h = 32,center = true);
			}
			translate([40,0,-baseHight/2 + 2]){
				motorMount();
				
				translate([45,0,20])
					cube([15,10,40],center = true);
			}
		}
		translate([57,0,-5])
			scale([1.2,1,1])
				motorDummy();
		rotate([0,0,90])
			translate([totalWidth/2 + 20,0,0])
				cube([50,30,20],center = true);
	}
}

module top(){
	rotate([0,180,0]){
		difference(){
			cylinder(d =totalWidth, h = skirt + 5,center = true,$fn=100);

			translate([0,0,-skirt/2 + 3])
				rotate_extrude(convexity = 10)
			translate([(baseWidth-ballD)/2 - margin, 0, 0])
				circle(d = ballDplusd,center = true,$fn = 32);
		
			translate([0,0,4])
				cylinder(d = baseWidth + 2*space,h = skirt + 5,center = true,$fn=128);
			
			translate([0,0,-skirt/2 + 2.5])
				rotate_extrude(convexity = 10)
					translate([indicatorExcentricity, 0, 0])
						square([7,5],center = true);
			translate([totalWidth/2,0,skirt/2 --2])
				cube(3,center = true);
			
			drillHole = 24;
			for(i = [0:drillHole -1])
				rotate([0,0,i*360/drillHole])
					translate([60,0,-11])
						cylinder(d1 = 1,d2 = 3,h = 1,center = true);
			
			for(i = [0:drillHole -1])
				rotate([0,0,i*360/drillHole])
					translate([30,0,-11])
						cylinder(d1 = 1,d2 = 3,h = 1,center = true);
		}
		
		translate([indicatorExcentricity,0,-skirt/2 ])
			scale([1,2.5,1.5])
				rotate([0,90,0])
					cylinder(d = 2, h = 8,center = true);
		translate([0,0,-(skirt- 12.5)])
			ring_gear (modul=1, tooth_number=160, width=20, randbreite=3, pressure_angle=20, schraegungswinkel=0);
	}
	translate([0,0,-((baseHight-4))/2 + skirt/2])
		bearingClamp(26,baseHight - 4);
}

module bearingClamp(d, h){
	difference(){
		cylinder(d = d,h = h,center = true);
		translate([0,0,-h/2 -1 + bearingHight]){
			scale([1.02,1.02,1.1])
				bearing();
			translate([0,18,0])
				cube([bearingOuterD -1,30,bearingHight + 2],center = true);
			translate([0,15,-5])
				cube([22,15,10],center = true);
			cylinder(d = bearingOuterD - 4, h = 18,center = true);
			translate([0,-10,0])
				rotate([90,0,0])
					cylinder(d = 3, h = 10,center = true);
			translate([0,8,-4])
				cube([18,20,25],center = true);
			cylinder(d = 10,h = 50,center = true);
		}
	}
}

module main(){
	translate([56,0,63/2 + 2])
//		motorDummy();
	translate([0,0,-baseHight/2 + 10])
		nema(true);
	intersection(){
		union(){
			translate([0,0,baseHight/2])
				bottom();
			translate([0,0,baseHight/2 + skirt/2  + 6])
				translate([0,0,ballD*2])
					top();
			
			translate([baseWidth/2 - ballD/2 - margin,0,baseHight + 0.5])
				sphere(d = ballD,center = true);
		}
		translate([-500,-0,-100])
			cube(1000);
	}
	
}

module bearing(){
	difference(){
		cylinder(d = bearingOuterD, h = bearingHight,center = true);
//		cylinder(d = bearingInnerD, h = bearingHight,center = true);
	}
}

module hexagon(d,h){
	cylinder(d = d,h = h,center = true,$fn = 6);
}

module bearingInletA(){
	difference(){
		union(){
			cylinder(d = bearingInnerD - .25,h = bearingHight + 3.5);
			cylinder(d = bearingInnerD +3,h = 3);
			translate([0,0,10.5])
				cylinder(d = bearingInnerD +2,h = 2);
		}
		translate([0,0,9]){
			cube([2,20,bearingHight + 5],center = true);
			cube([20,2,bearingHight + 5],center = true);
		}
		translate([0,0,0])
			hexagon(7,5);
		cylinder(d = 4,h = 25,center = true);
	}
}

module bearingInletB(){
	h = 1;
	difference(){
		union(){
			cylinder(d = bearingInnerD+ 5,h = h);
			translate([0,0,h +(2/4)]){
				cube([1.2,11,2],center = true);
				cube([11,1.2,2],center = true);
			}
		}
		cylinder(d = 4,h = 50,center = true);
	}
}

module pole(){
	difference(){
		union(){
			translate([0,0,5])
				cube([4,10,42],center = true);
			translate([23,0,-6])
				cube([42,4,20],center = true);
		}
		translate([0,0,10])
			rotate([0,90,0])
				cylinder(d = 3.5, h = 10,center = true);
		translate([0,0,20])
			rotate([0,90,0])
				cylinder(d = 3.5, h = 10,center = true);
	}
}

module motorMount(){
	difference(){
		union(){
			translate([0,-(42.3+8)/2,15]){
				pole();
				translate([0,42.3 +9,0])
					pole();
			}
			translate([25,0,12.5])
				cube([10,10,25],center = true);
		}
		translate([0,0,-3])
		motorDummy();//nemaX(false);
	}
}

module gear(){
	difference(){
		union(){
			spur_gear (modul=1, tooth_number=48, width=12, bohrung=4, pressure_angle=20, 	schraegungswinkel=0, optimized=false);
			cylinder(d = 11, h = 20);
		
			cylinder(d = 11, h = 20);
			translate([0,0,10])
				cylinder(d = 30,h = 15,center = true);
	}
	
	difference(){
		cylinder(d = 5.7, h = 60, center = true);
		translate([0,7,0])
			cube([10,10,60],center = true);
	}
	
	translate([0,12,15])
		rotate([90,0,0])
			cylinder(d = 3.2, h = 25,center = true);
	
	translate([0,6,15])
		cube([5.8,3,10],center = true);
	}
}

//Visualize:

//main();
//top();
//bottom();
//gear();
//bearingInletA();
bearingInletB();