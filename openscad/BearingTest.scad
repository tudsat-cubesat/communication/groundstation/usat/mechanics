include <data.scad>
$fn = 16;
module top(){
	
	difference(){
		union(){
			cylinder(d = outerDiameterBearing - 10, h = 15);
	
			cylinder(d = outerDiameterBearing + 4, h = he + 9);
		}
		translate([0,0,1])
			cylinder(d = outerDiameterBearing + 1, h = he + 1);
			
		cylinder(d = outerDiameterBearing -0.5, h =2);
		
		cylinder(d = 8, h = he + 11);
		
		cylinder(d = innerDiameterBearing  + 5, h = he + 6);
		
		translate([-0.5,-50,0])
			cube([1,100,he + 3]);
		
		rotate([0,0,90])
			translate([-0.5,-50,0])
				cube([1,100,he + 3]);
		
		rotate([0,0,-45])
			translate([-0.5,-50,0])
				cube([1,100,he + 3]);
		
		rotate([0,0,45])
			translate([-0.5,-50,0])
				cube([1,100,he + 3]);
				
		rotate([0,0,45/2])
		union(){
				translate([-0.5,-50,0])
			cube([1,100,he + 3]);
		
		rotate([0,0,90])
			translate([-0.5,-50,0])
				cube([1,100,he + 3]);
		
		rotate([0,0,-45])
			translate([-0.5,-50,0])
				cube([1,100,he + 3]);
		
		rotate([0,0,45])
			translate([-0.5,-50,0])
				cube([1,100,he + 3]);
			
		}
	}
}


module bottom(f){
	
	difference(){
		union(){
			translate([0,0,1])
			difference(){
				union(){
					cylinder(d = innerDiameterBearing, h = he+0.3);
		
					translate([0,0,he+0.3])
						cylinder(d =innerDiameterBearing + 2.5, h = 4);
				}	
			
				translate([-1,-15,1])
					cube([2,30,he+10]);

				translate([-15,-1,1])
					cube([30,2,he+10]);

		
				cylinder(d = 4, h = 100);
			}
		
			translate([0,0,-2])
				cylinder(d = innerDiameterBearing+4, h = 3);
			}
			
			translate([0,0,-2])
					cylinder(d = 7.8, h = 3, $fn = 6);
	}
}

module topX(){
	
			d = 0.5;
	difference(){
		union(){
			
			cylinder(d = outerDiameterBearing - 10, h =baseHeight - 2);
			
			cylinder(d = outerDiameterBearing + 8, h =baseHeight - 2);
			
			
			translate([0,0,-d-1])
				cylinder(d = outerDiameterBearing + 8, h = he + 9+d);
		}
		translate([0,0,1])
			cylinder(d = outerDiameterBearing + 1, h = he);
			
		translate([0,0,-d-1])
			#cylinder(d = outerDiameterBearing -3.5, h =5);
		
		cylinder(d = 10, h = he + 18);
		
		translate([0,0,-4])
		cylinder(d = innerDiameterBearing  + 7, h = he + 14);
		
		g = 22;
		translate([-g/2,-3,+1])
			cube([g,100,he ]);
		s = innerDiameterBearing + 7;
		translate([-s/2,0,4-2*he+1])
			cube([s,100,he*3 ]);
		
		
	translate([0,0,he/2 + 1])
		rotate([90,0,0])
		cylinder(d = 4, h = 100);
		
	
	}
}

/*
difference(){
//	rotate([180,0,0])
	translate([0,20,0])
		union(){
			top();
//			translate([0,40,0])
				bottom();
		}
	cube([100,1100,100]);
}*/
//topX();
/*
difference(){
		d = 0.5;
	translate([0,0,-d])
				cylinder(d = outerDiameterBearing + 8, h = he + 9+d);
	topX();
	translate([0,0,1])
			cylinder(d = outerDiameterBearing + 1, h = he);
			
		translate([0,0,-d])
			cylinder(d = outerDiameterBearing -3, h =2);
		
		cylinder(d = 8, h = he + 11);
		
		translate([0,0,-5])
		cylinder(d = innerDiameterBearing  + 19, h = 100);
	
	translate([-25,-40,-5])
		cube([50,50,50]);
}*/

//translate([0,0,12])
//rotate([0,180,0])
//bottom();
/*
$fn= 100;
difference(){
	union(){
		cylinder(d = 13, h = 0.5);
		
		translate([0,0,1])
		cube([10,1,2],center = true);
		
		translate([0,0,1])
		cube([1,10,2],center = true);
	}
	cylinder(d = 5, h = 5);
}
*/