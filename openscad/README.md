This directory contains all OpenScad (https://www.openscad.org/) files needed to build one unit. 

You can modify them if you want to do that.

To simplify working with these files the parameter fn is set to 16. This allows to quickly render an object but makes them angular.

To produce usable parts you should set it to at least 64.

