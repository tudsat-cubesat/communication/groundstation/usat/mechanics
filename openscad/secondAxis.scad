include <gears.scad>

//Nicho Modules

module nb(x,y,z){
	translate([0,0,z/2])
		cube([x,y,z],center = true);
}

module nc(d,h){
	cylinder(d = d, h = h);
}

module ncc(d,h){
	cylinder(d = d, h = h,center = true);
}

module edgeBox(b,t,h,r,f){
	hull(){
		translate([-(b/2 + r),-(t/2+r),0])
			nc(2*r,h,$fn=f);
		
		translate([-(b/2 + r),(t/2+r),0])
			nc(2*r,h,$fn=f);
		
		translate([(b/2 + r),-(t/2+r),0])
			nc(2*r,h,$fn=f);
		
		translate([(b/2 + r),(t/2+r),0])
			nc(2*r,h,$fn=f);
		
	}
	
}

//pylon parameters
wall = 3;
walls = 2* wall;
width = 90;
depth = 50+walls;
segHight = 70;
clearance = 0.5;
h = 7;
c = 2* clearance;

d_2 = 31/2;
l = 42;
w_ = 55;

$fn = 24;

fn2 = 32;

bearingInnerD = 8;
bearingOuterD = 22;
bearingHight = 7;


module adapterHole(){
	scale([0.95,0.95,1])
		edgeBox(width - c - walls,depth - c - walls,10,2,4);
}

module adapterMale(h = 10){
	difference(){
		edgeBox(width - c - walls,depth - c - walls,h,2,4);
		adapterHole();
	}
}

module adapterFemale(h = 10){
	difference(){
		edgeBox(width - walls,depth  - walls,h,2,4);
	}
}
	
module shape(h = 1){
	edgeBox(width,depth,h,2,4);
}

module base(h){
	centerOffset = -10;
	difference(){
		drillHole = 12;
		active = [0,4,8];// positions are depicted by the numbers of a clock
		for(i = [0:len(active) - 1]){
			hull(){
				translate([-15,0,0])
				nc(50,40);
				echo(active[i]);
				rotate([0,0,active[i]*360/drillHole])
					translate([60,0,0])
						nc(18,7,$fn = fn2);
					
				}
			}
			
		translate([centerOffset,0,0])
			scale([1,1,100])
				shape();
			
		for(i = [0:len(active)]){
			rotate([0,0,active[i]*360/drillHole])
				translate([60,0,0]){
					translate([0,0,7])
						nc(15,25,$fn = fn2);
						nc(4,30,$fn = fn2);
						nc(9,4,$fn = fn2);
				}
		}
	}
	translate([centerOffset,0,0])
		ringModule(h);
}
module ringModule(h){
	difference(){
		union(){
			hull(){
				shape();
			
				translate([0,0,h])
					shape();
					
				
			}
			translate([0,0,h - 1])
				adapterMale(5);
		}
		
		translate([0,0,h - 2.7])
			hull(){
				translate([0,0,7])
					scale([0.95,0.95,0.1])
						adapterHole();
				translate([0,0,-2])
					scale([1,1,0.001])
						adapterFemale();
		}
		
		hull(){
			adapterFemale();
			translate([0,0,h - 9.7 - 5])
				adapterFemale();
		}
		translate([0,0,h - 2])
			adapterHole();
	}
	
	translate([0,0,h-3]){
		translate([width/2 + 7,0,0])
			screwMountA();
	
		translate([-width/2 - 7,0,0])
			rotate([0,0,180])
				screwMountA();
	}
	
	translate([0,0,4])
		rotate([0,180,0]){
			translate([width/2 + 7,0,0])
				screwMountA();
	
			translate([-width/2 - 7,0,0])
				rotate([0,0,180])
					screwMountA();
		}
}

module screwMountA(){
	difference(){
		hull(){
			nb(10,20,4);
			translate([-3,0,-10])
				nb(.1,15,1);
		}
			
		translate([-5,0,-8])
			nb(20,11,10);
			
		translate([1.5,0,0])
			nc(4,10);
	}
}

module cap(h = 10){
	difference(){
		shape(h);
		adapterFemale(h -2);
	}
	
	translate([0,0,4])
		rotate([0,180,0]){
			translate([width/2 + 7,0,0])
				screwMountA();
	
			translate([-width/2 - 7,0,0])
				rotate([0,0,180])
					screwMountA();
		}
}

module holes(){
	drillHole = 24;
	for(i = [0:drillHole -1])
		rotate([0,0,i*360/drillHole])
			translate([60,0,0]){
				nc(3.5,30);
				nc(6,4);
			}
}

module holes2(){
	drillHole = 24;
	for(i = [0:drillHole -1])
		rotate([0,0,i*360/drillHole])
			translate([60,0,0]){
//				nc(4,30,$fn = fn2);
				nc(10,4,$fn = fn2);
			}
}

module bearing(){
	union(){
		
		cylinder(d = bearingOuterD + 0.4,h = bearingHight + 0.6,center = false);
		translate([0,0,-bearingHight/2])
			nc(bearingOuterD -5,bearingHight*2);
	}
}

module bearingHolder(){
	rotate([0,0,90])
		difference(){
			hull(){
				nb(12,30,bearingOuterD / 2 + 4);
				translate([-(13-1)/2,0,-13])
					nb(0.1,8,2);
			}
			translate([-(bearingHight + 0.6)/2,0,bearingHight*1.8 - 1])
				rotate([0,90,0])
					bearing();
			
			
			
			translate([0,0,-3])
				nb(13,2,10);
		}
}

module motorMountPlate(){
	translate([0,-walls,0]){
		difference(){
			nb(depth - 2*walls,l,6);
			translate([d_2,d_2,0])
				nc(3,10);
			translate([-d_2,d_2,0])
				nc(3,10);
			translate([d_2,-d_2,0])
				nc(3,10);
			translate([-d_2,-d_2,0])
				nc(3,10);
			
			nc(23,10);
			
			translate([0,l/2,0])
				nb(23,l,10);
		}
		
		translate([0,0,6])
			difference(){
				nb(depth + walls,l,5);
				nb(depth - 2*walls - 6,l,5);
				translate([d_2,d_2,0])
					nc(3,10);
				translate([-d_2,d_2,0])
					nc(3,10);
				translate([d_2,-d_2,0])
					nc(3,10);
				translate([-d_2,-d_2,0])
					nc(3,10);
		}
	}
}

module motorUnit(){
	heightOfAxis = 30;
	surplus = 10;
	
	axisMove = 16;

	difference(){
	union(){
		difference(){
			union(){
				ringModule(heightOfAxis + surplus);
				
				
				translate([axisMove,0,heightOfAxis])
					difference(){
						rotate([90,0,0])
							ncc(d = 26,h =width/2 +23);
						rotate([90,0,0])
							ncc(d = 100,h =width/2 +18);
						translate([0,0,-25]){
							translate([0,40,-7])
								rotate([50,0,0])
									nb(30,width/2+25,20);
							translate([0,-40,-7])
								rotate([-50,0,0])
									nb(30,width/2+25,20);
						}
						translate([0,0,10])
							nb(30,width/2+25,5);
					}
			}
			
			translate([axisMove,0,heightOfAxis]){
				rotate([90,0,0])
					cylinder(d = bearingInnerD +5,h = 200,center = true);
					nb(bearingInnerD + 5,200,2*surplus);
			}
		}
		translate([axisMove,0,heightOfAxis- bearingOuterD/2 - 0.5]){
			translate([0,-(depth/2 - 5),0])
				bearingHolder();
			
			translate([0,depth/2 - 5,0])
				rotate([0,0,180])
					bearingHolder();
		}
		
	
		translate([-l/2 + walls*2.5-7.5,0,0]){
			rotate([0,0,-90])
				motorMountPlate();
		}
		
		hull(){
			translate([43,-20,10])
				nb(9,13,20);
		
			translate([48,-20,4])
				nb(1,6,1);
		}
	}
		
	translate([35,-19,10])
			rotate([0,-90,-90])
				rotate([-90,0,0]){
					translate([15,0,10])
						cylinder(d = 2.5, h = 20,center = true);
					translate([5,0,10])
						cylinder(d = 2.5, h = 20,center = true);
		}
		
		translate([31,-19,15])
			nb(5,20,20);
}
}

module motorCap(){
	heightOfAxis = 30;
	surplus = 10;
	
	axisMove = 16;
	
	ex = 5;
	
	h = 20;
	l = 11.5;
	
	cap(h);

	difference(){
		union(){
			translate([axisMove,0,-l])
				nb(10.6,depth+ 3/2*walls - 1,l + 3);
			
			translate([axisMove,depth/2 + wall + ex +1,-l])
				hull(){
					rotate([90,0,0])
						ncc(30,5 + ex );
				
					translate([0,0,29.5])
						nb(1,5 + ex ,2);
					
					translate([0,0,-15])
						nb(30,5 + ex,2);
				}
				
			translate([axisMove,-(depth/2 + wall + ex + 1),-l])
				hull(){
					rotate([90,0,0])
						ncc(30,5 + ex);
				
					translate([0,0,29.5])
						nb(1,5 + ex,2);
					
					translate([0,0,-15])
						nb(30,5 + ex,2);
				}
		}
		
		translate([axisMove,0,-l])
			rotate([90,0,0])
				ncc(12,1000);
		
		translate([axisMove,0,-l - 20])
			nb(12,1000,20);
		
		translate([0,0,-150])
			nb(60,depth + wall - 1,300);
		
		translate([axisMove,depth/2 + wall + 2,-l])
			rotate([90,0,0])
				ncc(28,8 + ex + 2);
		
		translate([axisMove,-(depth/2 + wall + 2),-l])
			rotate([90,0,0])
				ncc(28,8 +ex + 2);
		
		translate([axisMove,0,-l])
			translate([0,0,-25])
				nb(28,81,25);
	}
}


module motorCap2(){
	heightOfAxis = 30;
	surplus = 10;
	
	axisMove = 16;
	
	ex = 5;
	
	h = 20;
	l = 11.5;
	
	union(){
		cap(h);

		translate([axisMove,depth/2 + wall + ex +1,-l])
			rotate([0,0,180])
				ear();
			
		translate([axisMove,-(depth/2 + wall + ex +1),-l])
				ear();

		translate([axisMove,23,-5])
			nb(20,7,5);
		
		translate([axisMove,23,8]){
			rotate([0,50,0])
				nb(25,7,2);
			
			rotate([0,-50,0])
				nb(25,7,2);
		}
	}
}

module ear(){
	heightOfAxis = 30;
	surplus = 10;
	
	axisMove = 16;
	
	ex = 5;
	
	h = 20;
	l = 11.5;
	
	difference(){
		hull(){
			rotate([90,0,0])
				ncc(30,5 + ex );
				
			translate([0,0,29.5])
				nb(1,5 + ex ,2);
					
			translate([0,0,-15])
				nb(30,5 + ex,2);
		}

		rotate([90,0,0])
			ncc(12,1000);
		
		translate([0,0, - 20])
			nb(12,1000,20);
		
		translate([0,0.5,-25])
				nb(28,9.5,25);
				
		translate([0,0.75,0])
		rotate([90,0,0])
			ncc(28,10);
				
		
		
	}
}

module wormGear(){
	difference(){
		union(){
			translate([0,0,-3])
			worm(modul=2, gangzahl=2, length=35, bohrung=0, pressure_angle=20, steigungswinkel=10, zusammen_gebaut=true);
				
			translate([0,0,-6])
			nc(18,5);
		}
		
		translate([0,0,25]){
			translate([0,2,0])
				nb(2,10,10);
			nb(10,2,10);
		}
		
		translate([0,13,-5])
			nb(25,10,5);
		
		translate([0,4,0])
			cube([5.7,3,15],center = true);
		
		translate([0,10,-2])
			rotate([90,0,0])
				cylinder(d = 3,h = 20,center = true);
		difference(){
				cylinder(d = 5.7, h = 40, center = true);
				translate([0,7,0])
					cube([10,10,60],center = true);
			}
	}
}

module gear(){
	difference(){
		union(){
			spur_gear (modul=2, tooth_number=24, width=10, bohrung=0, pressure_angle=10, schraegungswinkel=-10, optimized=false);
			
			translate([0,0,10])
			rotate([90,0,0])
			difference(){
				scale([1.3,1,1])
					ncc(d = 4,h = 43,$fn = 4);
				ncc(d = 8,h = 15);
			}
		}
			ncc(d = 8,h = 20);
	}
}

module endStop(){
	cube([20,6.25,10.1]);

	translate([0,5,5]){
		rotate([-90,0,0]){
			translate([15,0,10])
				cylinder(d = 2.5, h = 30,center = true);
			translate([5,0,10])
				cylinder(d = 2.5, h = 30,center = true);
		}
	}

	translate([0,0,10])
		rotate([0,-15,0])
			cube([22,5,0.5]);


	translate([2,0,-4]){
		cube([1,3,4]);
		translate([6,0,0])
			cube([1,3,4]);
		translate([9+6,0,0])
			cube([1,3,4]);
	}

	translate([20,0,17])
		rotate([-90,0,0])
			cylinder(d = 3.5, h = 4);
}


module goproMount(){
	difference(){
		union(){
			translate([-3,0,0])
				nb(20,14,20);
			translate([-0.5,0,20])
				nb(15,14,20);
		}
		
		translate([0,0,7])
			rotate([90,0,0])
				ncc(d = 8.5,h = 20);
		
		
		translate([-8,0,4.5])
			nb(20,14,5);
		
		translate([-8,0,7])
			ncc(d = 4,h = 20);
		
		translate([-8,0,12])
			nb(10,5.7,3);
		
		translate([-0.5,0,30]){
			difference(){
				nb(15,14,10);
				rotate([90,0,0])
					ncc(d = 15, h = 100);
			}
			rotate([90,0,0])
				ncc(d = 5.5, h = 100);
			nb(20,3,20);
		}
		translate([0,0,20]){
			nb(20,3,20);
			translate([0,6,0])
				nb(20,3,20);
			translate([0,-6,0])
				nb(20,3,20);
		}
	}
}
module cableModule(){
	d = 20;
	d_t = 25;
	d_x = depth;
	
	difference(){
		ringModule(50);
		
		translate([d_x,0,d_t])
			rotate([0,90,0])
				scale([0.6,1,1])
					ncc(d = d, h = 20);
		
	}
	translate([d_x,0,d_t])
		rotate([0,90,0])
	
					scale([0.6,1,1])
			difference(){
				
				union(){
						ncc(d = d, h = 20);
					
					translate([0,0,1])
						ncc(d = d + 4, h = 2);
					
					translate([0,0,8])
						ncc(d = d + 4, h = 2);
				}
				ncc(d = d-3, h = 20);
			}
	}
	

module tubes(){
	difference(){
		union(){
			nc(11,11);
			nc(25,1);
		}
		nc(8.7,11);
	}
	translate([20,0,0])
		difference(){
			nc(10,10);
			nc(8.5,15);
	}
}


module hand2(){
	difference(){
		union(){
			nc(50,3);
			
			cylinder(d1 = 25,d2 = 20,20);
			
		}
		
		nc(9,20);
		
		translate([0,0,3])
			nb(13,30,7.5);
		
		difference(){
			nc(40,3);
			nc(30,3);
			nb(50,10,3);
		}
	}
}

module yagiAdapter(){
	
	difference(){
		union(){
			nc(50,3);
		}
		
		angles =[20,-20,-200,-160];
		for(i = [0:len(angles)-1]){
			rotate([0,0,angles[i]])
				translate([0,35/2,0])
					nc(5,20);
		}
		
		ncc(8,5);
	}
	
	difference(){
		nb(60,22,12);
		
		translate([0,0,16])
			rotate([0,90,0,0])
				ncc(25,70);
		
		translate([26,0,0.5])
			rotate([90,0,0,0])
				ncc(4,70);
		
		translate([-26,0,0.5])
			rotate([90,0,0,0])
				ncc(4,70);
		
		ncc(8,5);
	}
}


// VISUALIZATION: Double slash indicate comments, remove them next to the 
//desired part, hit F6 and wait.

//wormGear();
//gear();

//motorCap();
//motorUnit();
//cableModule();
//tubes();
//ringModule(100);//change the numeric value to a desired one (unit mm)
//base(100);//change the numeric value to a desired one (unit mm)

//goproMount();
//hand2();
//yagiAdapter()